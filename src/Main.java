import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {

        String command;
        RandomStrategy randomStrategy=new RandomStrategy();
        FileStrategy fileStrategy=new FileStrategy();
        StdInStrategy stdInStrategy=new StdInStrategy();
        Scanner sc = new Scanner(System.in);
        InputReader inputReader=new InputReader();


        System.out.println("Witaj!");
        System.out.println("Podaj typ wejścia: (1-random, 2-stdin, 3-plik)");


        command=sc.nextLine();

        if (command.equals("1")){
            inputReader.setStrategy(randomStrategy);
        }
        else if (command.equals("2")){
            inputReader.setStrategy(stdInStrategy);
        }
        else if (command.equals("3")){
            inputReader.setStrategy(fileStrategy);
        }


        while (true){
            System.out.println("Podaj komendę (1 - next int, 2- next string, 3 - next double, quit - wyjście z programu");
            command=sc.nextLine();
            if(command.equals("quit")){
                break;
            }
            else if (command.equals("1")){
                System.out.println(inputReader.requestInt());
            }
            else if (command.equals("2")){
                System.out.println(inputReader.requestString());
            }
            else if (command.equals("3")){
                System.out.println(inputReader.requestDouble());
            }
        }

    }
}
