import java.util.Scanner;

/**
 * Created by RENT on 2017-08-17.
 */
public class StdInStrategy implements IInputStrategy{
    private Scanner scanner = new Scanner(System.in);
    @Override
    public int getInt() {
        System.out.println("Podaj liczbę typu int");
        int count = scanner.nextInt();
        return count;
    }

    @Override
    public String getString() {
        System.out.println("Podaj łańcuch zanków");
        String sentence = scanner.nextLine();
        return sentence;
    }

    @Override
    public double getDouble() {
        System.out.println("Podaj liczbę typu double");
        double count = scanner.nextDouble();
        return count;
    }
}
