public class InputReader {
    private IInputStrategy strategy;

    public InputReader() {
    }

    public void setStrategy(IInputStrategy strategy) {
        this.strategy = strategy;
    }

    public int requestInt(){
        return strategy.getInt();
    }
    public String requestString(){
        return strategy.getString();
    }
    public double requestDouble(){
        return strategy.getDouble();
    }
}
