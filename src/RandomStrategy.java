import java.util.Random;

/**
 * Created by Marek on 17.08.2017.
 */
public class RandomStrategy implements IInputStrategy {
    private Random random=new Random();
    private String[] strings={"asd","avv","dosda","ppp"};
    private String randomString="";
    private StringBuilder stringBuilder=new StringBuilder();
    private char c;



    @Override
    public int getInt() {
        return random.nextInt(10);
    }

    @Override
    public String getString() {

        randomString="";
        stringBuilder.setLength(0);
        int lengthOfRandomString = random.nextInt(10);
        for (int i = 0; i < lengthOfRandomString; i++) {
            c=(char)(random.nextInt(100)+100);
            randomString=stringBuilder.append(c).toString();
        }
        return randomString;
    }

    @Override
    public double getDouble() {
        return (10*random.nextDouble());
    }
}
