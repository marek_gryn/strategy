import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.lang.reflect.Parameter;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-17.
 */
public class FileStrategy implements IInputStrategy {
    private final File file = new File("someFile.txt");
    private final Scanner scanner = new Scanner(new FileReader(file));
    private final PrintWriter writer = new PrintWriter(file);

    public FileStrategy() throws FileNotFoundException {
        writer.print(12);
        writer.print("aasdf");
        writer.close();

    }

    @Override
    public int getInt() {
        int count = scanner.nextInt();
        return count;
    }

    @Override
    public String getString() {
        String sentence = scanner.next();
        return sentence;
    }

    @Override
    public double getDouble() {
        double count = scanner.nextDouble();
        return count;
    }

    public void closeReader(){
        scanner.close();
    }
}
